import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';

import DashboardIcon from '@material-ui/icons/Dashboard';
import ComputerIcon from '@material-ui/icons/Computer';
import InfoIcon from '@material-ui/icons/Info';
import ListItem from '@material-ui/core/ListItem';

import { Link } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';

const drawerWidth = 65;

const styles = (theme: any) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#191919"
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  }
});

const css = {
    listItemStyle: {
        padding: "0px",
    },
    iconButtonStyle: {
        color: "#DC9202"
    }
}
class Sidebar extends React.Component<any, any> {


    render(){
        const { classes } = this.props;

        return (
            <Drawer classes={{
                paper: classes.drawerPaper,
              }} variant="permanent" anchor="left">
                <List style ={{backgroundColor: "#191919", height:"100%"}}>
                    <ListItem style={css.listItemStyle}  >
                        <Link to="/dashboard">
                            <Tooltip title="Dashboard" placement="right-end">
                                <IconButton component="span">
                                    <DashboardIcon style={css.iconButtonStyle} fontSize="large" />
                                </IconButton>
                            </Tooltip>
                        </Link>
                    </ListItem>
                    <ListItem style={css.listItemStyle} >
                        <Link to="/terminal">
                            <Tooltip title="Main" placement="right-end">
                                <IconButton component="span">
                                    <ComputerIcon style={css.iconButtonStyle} fontSize="large" />
                                </IconButton>
                            </Tooltip>
                        </Link>
                    </ListItem>
                    <ListItem style={css.listItemStyle}  >
                        <Link to="about">
                            <Tooltip title="About" placement="right-end">
                                <IconButton component="span">
                                    <InfoIcon style={css.iconButtonStyle} fontSize="large" />
                                </IconButton>
                            </Tooltip>
                        </Link>
                    </ListItem>
 
              </List>
        
          </Drawer>
        );
    }
  }

export default withStyles(styles)(Sidebar);
  