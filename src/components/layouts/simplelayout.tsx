import * as React from 'react';
import * as FlexLayout from 'flexlayout-react';
import Main from '../main';
// import 'flexlayout-react/style/dark.css';
import './dark.css';

var json = {
    global: {},
    layout: {
        "type": "row",
        "weight": 100,
        "children": [
            {
                "type": "tabset",
                "weight": 50,
                "selected": 0,
                "children": [
                    {
                        "type": "tab",
                        "name": "FX",
                        "component": "main"
                    }
                ]
            },
            {
                "type": "tabset",
                "weight": 50,
                "selected": 0,
                "children": [
                    {
                        "type": "tab",
                        "name": "FI",
                        "component": "button"
                    }
                ]
            },
            {
                "type": "tabset",
                "weight": 50,
                "selected": 0,
                "children": [
                    {
                        "type": "tab",
                        "name": "FI",
                        "component": "main"
                    }
                ]
            }
        ]
    }
};

var json2 = {
	"global": {
		"splitterSize": 8,
		"enableEdgeDock": true,
		"tabSetEnableMaximize": true,
		"tabEnableClose": true,
		"tabSetEnableTabStrip": true
	},
	"layout": {
		"type": "row",
		"id": "#4",
		"children": [
			{
				"type": "tabset",
				"weight": 12.5,
				"active": true,
				"id": "#5",
				"children": [
					{
						"type": "tab",
						"name": "FX",
						"component": "grid",
						"id": "#6"
					}
				]
			},
			{
				"type": "tabset",
				"weight": 25,
				"id": "#7",
				"children": [
					{
						"type": "tab",
						"name": "FI",
						"component": "grid",
						"id": "#8"
					}
				]
			}
		]
	},
	"borders": [
		 {
		    "type": "border",
		 	"location": "left",
			"children": [
				{
					"type": "tab",
					"enableClose":false,
					"name": "Navigation",
					"component": "grid",
					"id": "#24"
				}
			]
		},
		{
		    "type": "border",
		 	"location": "right",
			"children": [
				{
					"type": "tab",
					"enableClose":false,
					"name": "Options",
					"component": "grid",
					"id": "#3"
				}
			]
		},
		{
		    "type": "border",
			"location": "bottom",
			"children": [
				{
					"type": "tab",
					"enableClose":false,
					"name": "Activity Blotter",
					"component": "grid",
					"id": "#2"
				},
				{
					"type": "tab",
					"enableClose":false,
					"name": "Execution Blotter",
					"component": "grid",
					"id": "#1"
				}
			]
		}
	]
}

export class SimpleLayout extends React.Component<any, any> {
    

    constructor(props) {
        super(props);
        // this.layout = React.createRef();
        this.state = {model: FlexLayout.Model.fromJson(json2)};
    }
    factory = (node) => {
        var component = node.getComponent();
        if (component === "text") {
            return <div dangerouslySetInnerHTML={{__html:node.getConfig().text}}/>;
        } else if (component === "main"){
            return <Main />;
        }
    }

    onAdd = () => {
        FlexLayout.Layout.prototype.addTabToTabSet(
            "#5",
            {
                component: "text",
                name: "added",
                config: {text: "i was added"}
            }
        )
    }

    onAdd2 = () => {
        if (this.state.model!.getMaximizedTabset() == null) {
            (this.refs.layout as FlexLayout.Layout).addTabWithDragAndDropIndirect("Add grid<br>(Drag to location)", {
                component: "grid",
                name: "a new grid"
            }, this.onAdd2);
            this.setState({ adding: true });
        }
    }

  
    render() {
       // console.log(FlexLayout.Model.prototype.getActiveTabset());
       console.log(this.state.model);
        return (
            <React.Fragment>

            <div className="toolbar">
            <button onClick={this.onAdd2}> add </button>
            </div>
                <div className="inner">
                    <FlexLayout.Layout ref="layout"
                                       model={this.state.model}
                                       factory={this.factory}/>
                </div>
          
            </React.Fragment>
        );
    }
}
