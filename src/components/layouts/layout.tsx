import * as React from "react";
import * as FlexLayout from "flexlayout-react";
import { Node, TabSetNode, TabNode, DropInfo, BorderNode } from "flexlayout-react";
import { marketLayout } from './default';
import  Main from "../main";
import AgGrid from '../grid';
import './dark.css';
import {dowjones_symbols} from '../../data/staticdata';
import {KeyData} from '../../data/KeyData';
import PortfolioGrid from "../Grids/PortfolioGrid";
import LivepriceGrid from "../Grids/LivepriceGrid";
import TerminalHOC from "../realtime/terminalHOC";
import Button from '@material-ui/core/Button';

const alpha = require('alphavantage')({ key: '97420TWT6AHY11JV' });

var fields = ["Name", "ISIN", "Bid", "Ask", "Last", "Yield"];

interface ILayoutState {
    layoutFile: string | null;
    model: FlexLayout.Model | null;
    adding: boolean;
    keyLiveData: any;
    allCurrPrice: any;
}

export class Layout extends React.Component<any, ILayoutState> {

    loadingLayoutName?: string;

    constructor(props:any) {
        super(props);
        this.state = { 
            layoutFile: null, 
            model: FlexLayout.Model.fromJson(marketLayout), 
            adding: false,
            
            keyLiveData: null,
            allCurrPrice: null
        };
        this.getAllCurrentPricesDowJones();
        console.log('layout...');
        console.log(this.state.keyLiveData);
        console.log(this.state.allCurrPrice);
    }
// --------------------------------------------------------------------------------------------------------------- ALPHAVANTAGE

combinedKeyLiveData = () => {
    var arr: any = [];
    for ( var i in KeyData) {
        for ( var j in this.state.allCurrPrice) {
            var sym = Object.values(this.state.allCurrPrice[j])[0];
            const livepriceArr: string[] =  Object.values(this.state.allCurrPrice[j]);
            const livepriceStr: string = livepriceArr[1];
            const liveprice: number = parseFloat(livepriceStr);
            // console.log(varint)
            //console.log(typeof varint);
            //console.log(sym);

           // console.log(KeyData[i].symbolCaps)
            if (KeyData[i].symbolCaps === sym) {
                KeyData[i]['liveprice'] = liveprice
               // console.log(liveprice);
               // console.log(KeyData[i]);
                arr.push(KeyData[i]);
            }
        }
    }
    console.log(arr);
    return arr
}
getAllCurrentPricesDowJones = () => {
    alpha.data.batch(dowjones_symbols).then((data: any) => {
      this.setState({allCurrPrice: data["Stock Quotes"]}, 
            () => {
                const enhancedData = this.combinedKeyLiveData();
                this.setState({keyLiveData: enhancedData})
            }
        )
    });
}

// --------------------------------------------------------------------------------------------------------------- CAPLIN FLEXLAYOUT
    allowDrop = (dragNode:(TabNode | TabSetNode), dropInfo:DropInfo) => {
        let dropNode = dropInfo.node;

        // prevent non-border tabs dropping into borders
        if (dropNode.getType() === "border" && (dragNode.getParent() === null || dragNode.getParent()!.getType() !== "border"))
            return false;

        // prevent border tabs dropping into main layout
        if (dropNode.getType() !== "border" && (dragNode.getParent() !== null && dragNode.getParent()!.getType() === "border"))
            return false;

        return true;
    }

    error = (reason:string) => {
        alert("Error loading json config file: " + this.loadingLayoutName + "\n" + reason);
    }

    onAddClick = (event:React.MouseEvent) => {
        if (this.state.model!.getMaximizedTabset() == null) {
            (this.refs.layout as FlexLayout.Layout).addTabWithDragAndDropIndirect("ADD COMPONENT <br>(DRAG ME)", {
                component: "main",
                name: "a new grid"
            }, this.onAdded);
            this.setState({ adding: true });
        }
    }

    
    onAddTerminal = (event:React.MouseEvent) => {
        if (this.state.model!.getMaximizedTabset() == null) {
            (this.refs.layout as FlexLayout.Layout).addTabWithDragAndDropIndirect("ADD <br> TERMINAL <br>(DRAG ME)", {
                component: "TerminalHOC",
                name: "Terminal"
            }, this.onAdded);
            this.setState({ adding: true });
        }
    }

    onAddLivePriceGrid = (event:React.MouseEvent) => {
        if (this.state.model!.getMaximizedTabset() == null) {
            (this.refs.layout as FlexLayout.Layout).addTabWithDragAndDropIndirect("ADD <br> LIVE PRICE GRID <br>(DRAG ME)", {
                component: "LivepriceGrid",
                name: "LIVE PRICES"
            }, this.onAdded);
            this.setState({ adding: true });
        }
    }

    onAddPortfolioGrid= (event:React.MouseEvent) => {
        if (this.state.model!.getMaximizedTabset() == null) {
            (this.refs.layout as FlexLayout.Layout).addTabWithDragAndDropIndirect("ADD <br> PORTFOLIO GRID <br>(DRAG ME)", {
                component: "PortfolioGrid",
                name: "PORTFOLIO"
            }, this.onAdded);
            this.setState({ adding: true });
        }
    }

    onAdded = () => {
        this.setState({ adding: false });
    }

    onTableClick = (node:Node, event:Event) => {
        //console.log("tab: \n" + node._toAttributeString());
        // console.log("tabset: \n" + node.getParent()!._toAttributeString());
    }

    factory = (node:TabNode) => {
        var component = node.getComponent();

        if (component === "grid") {
            if (node.getExtraData().data == null) {
                // create data in node extra data first time accessed
                node.getExtraData().data = this.makeFakeData();
            }

            return <SimpleTable fields={fields} onClick={this.onTableClick.bind(this, node)} data={node.getExtraData().data} />;
        }
        else if (component === "sub") {
            var model = node.getExtraData().model;
            if (model == null) {
                node.getExtraData().model = FlexLayout.Model.fromJson(node.getConfig().model);
                model = node.getExtraData().model;
                // save submodel on save event
                node.setEventListener("save", (p:any) => {
                     this.state.model!.doAction(FlexLayout.Actions.updateNodeAttributes(node.getId(), {config:{model:node.getExtraData().model.toJson()}}));
                    //  node.getConfig().model = node.getExtraData().model.toJson();
                }
                );
            }

            return <FlexLayout.Layout model={model} factory={this.factory} />;
        }
        else if (component === "text") {
            return <div dangerouslySetInnerHTML={{ __html: node.getConfig().text }} />;
        }

        else if (component === "LivepriceGrid") {
            return <LivepriceGrid keyLiveData={this.state.keyLiveData} />;
        }

        else if (component === "PortfolioGrid") {
            return <PortfolioGrid keyLiveData={this.state.keyLiveData} />;
        }

        else if (component === "TerminalHOC") {
            return <TerminalHOC />;
        }
        else if (component === "main"){
            return <Main />;
        }

        return null;
    }

   
    render() {
        console.log('layout render')
        var onRenderTab = function (node:TabNode, renderValues:any) {
            //renderValues.content += " *";
        };

        var onRenderTabSet = function (node:(TabSetNode|BorderNode), renderValues:any) {
            //renderValues.headerContent = "-- " + renderValues.headerContent + " --";
            //renderValues.buttons.push(<img src="images/grey_ball.png"/>);
        };

        let contents: React.ReactNode = "loading ...";
        if (this.state.model !== null) {
            contents = <FlexLayout.Layout
                ref="layout"
                model={this.state.model}
                factory={this.factory}
                onRenderTab={onRenderTab}
                onRenderTabSet={onRenderTabSet} 
                />;
        }

        return (
            <React.Fragment>
                <div className="toolbar">
                    <Button onClick={this.onAddTerminal} style={{height: "30px", margin:"2px",backgroundColor: "#DC9202", color: "#222"}} >
                        Add Terminal
                    </Button>
                    <Button onClick={this.onAddLivePriceGrid} style={{height: "30px", margin:"2px",backgroundColor: "#DC9202", color: "#222"}} >
                        Add Live Prices
                    </Button>
                    <Button onClick={this.onAddPortfolioGrid} style={{height: "30px", margin:"2px",backgroundColor: "#DC9202", color: "#222"}} >
                        Add Portfolio
                    </Button>
                </div>
                <div className="contents">
                    {contents}
                </div>
            </React.Fragment>
        );
    }

    makeFakeData() {
        var data: any = [];
        var r = Math.random() * 50;
        for (var i = 0; i < r; i++) {
            var rec: { [key: string]: any; } = {};
            rec.Name = this.randomString(5, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
            rec.ISIN = rec.Name + this.randomString(7, "1234567890");
            for (var j = 2; j < fields.length; j++) {
                rec[fields[j]] = (1.5 + Math.random() * 2).toFixed(2);
            }
            data.push(rec);
        }
        return data;
    }

    randomString(len:number, chars:string) {
        var a:any = [];
        for (var i = 0; i < len; i++) {
            a.push(chars[Math.floor(Math.random() * chars.length)]);
        }

        return a.join("");
    }
}

class SimpleTable extends React.Component<{ fields: any, data: any, onClick: any }, any> {
    shouldComponentUpdate() {
        return true;
    }

    render() {
        var headercells = this.props.fields.map(function (field:any) {
            return <th key={field}>{field}</th>;
        });

        var rows: any = [];
        for (var i = 0; i < this.props.data.length; i++) {
            var row = this.props.fields.map((field:any) => <td key={field}>{this.props.data[i][field]}</td>);
            rows.push(<tr key={i}>{row}</tr>);
        }

        return <table className="simple_table" onClick={this.props.onClick}>
            <tbody>
                <tr>{headercells}</tr>
                {rows}
            </tbody>
        </table>;
    }
}
