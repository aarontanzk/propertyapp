import React from 'react';
import Sidebar from '../Sidebar';
import {SimpleLayout } from './simplelayout';
import {Layout} from './layout';

export class LayoutHOC extends React.Component<any, any> {
    render(){
        console.log('layouthoc');
        return (
            <React.Fragment>
                <Sidebar />
                <Layout />
            </React.Fragment>
        );
    }
}
