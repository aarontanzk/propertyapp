import React from 'react';
import {dowjones_symbols} from '../data/staticdata';
import '../App.css';
import Sidebar from './Sidebar';
import { Typography } from "@material-ui/core";
import Divider from '@material-ui/core/Divider';
// Simple examples


export class About extends React.Component<any, any> {
    constructor(props: any){
      super(props);
    }
    render(){

        return (
        <React.Fragment>
            <Sidebar/>
            <div className="Container">        
                <div style={{padding:"100px"}}>
                <Typography variant="h1" style={{padding: "10px",color: "#DC9202"}}>
                    Market Data Analytics Platform
                </Typography>
                <Divider style={{backgroundColor:"#DC9202"}} />
                <Typography variant="h3" style={{padding: "10px",color: "#DC9202"}}>
                    Components:
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - Dashboard Layout Manager
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - Terminal
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - Live Price Grid
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - Trade Blotter
                </Typography>
                <Divider style={{backgroundColor:"#DC9202"}} />
                <Typography variant="h3" style={{padding: "10px",color: "#DC9202"}}>
                    TODO:
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - Order Blotter
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - Portfolio Risk Analytics
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - Portfolio Engineering Tools
                </Typography>
                <Divider style={{backgroundColor:"#DC9202"}} />
                <Typography variant="h3" style={{padding: "10px",color: "#DC9202"}}>
                    Dev Tools:
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - React
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - Material UI
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - AlphaVantage API
                </Typography>
                <Typography variant="h4" style={{padding: "10px",color: "#DC9202"}}>
                    - VictoryCharts JS
                </Typography>
                </div>
            </div>
        </React.Fragment>
        );
    }
}
  