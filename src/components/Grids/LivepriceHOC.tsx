import React from 'react';
import Sidebar from '../Sidebar';
import LivePriceGrid from './LivepriceGrid';

import {dowjones_symbols} from '../../data/staticdata';
import {KeyData} from '../../data/KeyData';
import PortfolioGrid from "./PortfolioGrid";
import LivepriceGrid from "./LivepriceGrid";
import { Loading } from '../Loading';

const alpha = require('alphavantage')({ key: '4OY7RBA1R7HMIPFR' });

export class LivepriceHOC extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state={
            keyLiveData: null
        }
    this.getAllCurrentPricesDowJones();
    }
    combinedKeyLiveData = () => {
        var arr: any = [];
        for ( var i in KeyData) {
            for ( var j in this.state.allCurrPrice) {
                var sym = Object.values(this.state.allCurrPrice[j])[0];
                const livepriceArr: string[] =  Object.values(this.state.allCurrPrice[j]);
                const livepriceStr: string = livepriceArr[1];
                const liveprice: number = parseFloat(livepriceStr);
                // console.log(varint)
                //console.log(typeof varint);
                //console.log(sym);
    
               // console.log(KeyData[i].symbolCaps)
                if (KeyData[i].symbolCaps === sym) {
                    KeyData[i]['liveprice'] = liveprice
                   // console.log(liveprice);
                   // console.log(KeyData[i]);
                    arr.push(KeyData[i]);
                }
            }
        }
        console.log(arr);
        return arr
    }

    getAllCurrentPricesDowJones = () => {
        alpha.data.batch(dowjones_symbols).then((data: any) => {
          this.setState({allCurrPrice: data["Stock Quotes"]}, 
                () => {
                    const enhancedData = this.combinedKeyLiveData();
                    console.log('set state liveprice hoc')
                    this.setState({keyLiveData: enhancedData})
                }
            )
        });
    }

    render(){
        console.log('livepricehoc')
        console.log(this.state.keyLiveData)
    
            return (
                <React.Fragment>
                    <Sidebar />
                    <div style={{marginLeft: "65px"}}>
                        <LivepriceGrid keyLiveData={this.state.keyLiveData} />
                    </div>
                </React.Fragment>
            );
        
     
    
    }
}
