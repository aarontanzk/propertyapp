import React from 'react';
import {AgGridReact} from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import './ag-grid.css';

import '../../App.css';
import {colDefs} from '../../data/columns';
import { Loading } from '../Loading';

export default class LivePriceGrid extends React.Component<any, any> {
    private gridApi: any;
    private gridColumnApi: any;
    
    constructor(props: any){
      super(props);

      this.state = {
        defaultColDef: {
            enableRowGroup: true,
            enablePivot: true,
            enableValue: true,
            width: 100,
            sortable: true,
            resizable: true,
            filter: true,
            editable: false
        },
        columnDefs: colDefs,
        rowData: this.props.keyLiveData,
        rowSelection: "multiple"
      }
    }

    onGridReady = params => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    render(){
      console.log('live price grid')
      console.log(this.props.keyLiveData)

      if (this.state.rowData !== null) {
        return (
          <React.Fragment>
            <div
              className="ag-theme-dark"
              style={{
                height: '100vh',
                width: '100%',
                // marginLeft: "66px"
              }}
            >
              <AgGridReact
                columnDefs={this.state.columnDefs}
                rowData={this.state.rowData}
                defaultColDef={this.state.defaultColDef}
                animateRows={true}
                rowSelection={this.state.rowSelection}
                //sideBar={this.state.sideBar}
                //statusBar={this.state.statusBar}
                //enableRangeSelection={true}
                onGridReady={this.onGridReady}
                  >
              </AgGridReact>
            </div>
          </React.Fragment>
        );
      } else {
        return (
           <Loading/>
        )
      }
  }

}
  
