import React from 'react';
import {AgGridReact} from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import './ag-grid.css';
//import "ag-grid-enterprise";
import GenderCellRenderer from "./genderCellRenderer";
// import '../../App.css';
import {portfolioColDefs} from '../../data/columns';
import { PortfolioAddDialog } from './PortfolioAddDialog';
import {IKeyLiveDataObject } from '../../data/KeyData';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/DeleteForever';
import ClearAllIcon from '@material-ui/icons/DeleteSweep';
import Typography from '@material-ui/core/Typography';

var moment = require('moment');
export default class PortfolioGrid extends React.Component<any, any> {
    private gridApi: any;
    private gridColumnApi: any;
    private newCount = 1;

    constructor(props: any){
      super(props);

      this.state = {
        initialized: false,
        portfolioValue: 0,
        dialogOpen: false,
        frameworkComponents: { genderCellRenderer: GenderCellRenderer },
        defaultColDef: {
            enableRowGroup: true,
            enablePivot: true,
            enableValue: true,
            width: 100,
            sortable: true,
            resizable: true,
            filter: true
        },
        columnDefs: portfolioColDefs,
        rowData: [],
        rowSelection: "multiple"
      }
      
    }

    dialogOpen = () => {
      this.setState({dialogOpen: true})
    }

    dialogClose = () => {
      this.setState({dialogOpen: false})
    }
    
    onGridReady = params => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    };
    
    getRowData = () => {
        var rowData: any = [];
        this.gridApi.forEachNode(function(node) {
          rowData.push(node.data);
        });
        console.log("Row Data:");
        console.log(rowData);
    }

    getPortfolioValue = () => {
      if(this.gridApi !== undefined) {
      var totalValue: number = 0;
      var rowData: any = [];
      this.gridApi.forEachNode(function(node) {
        rowData.push(node.data);
      });
      console.log("Row Data:");
      console.log(rowData);

      for ( var i in rowData) {
        totalValue += rowData[i].value;
      }
      this.setState({portfolioValue: totalValue})
      // return totalValue;
    }
    //return 0;
  }

    clearData = () => {
        this.gridApi.setRowData([]);
    }

    onAddRow = () => {
        var newItem = this.createNewRowData();
        var res = this.gridApi.updateRowData({ add: [newItem] });
        this.printResult(res);
        this.getPortfolioValue();
    }

    generateInitialData = () => {
      console.log(this.gridApi)
      if (this.gridApi !== undefined) {
        var newItem1 = this.createNewRowData();
        var newItem2 = this.createNewRowData();
        var newItem3 = this.createNewRowData();
        var newItem4 = this.createNewRowData();
        var newItem5 = this.createNewRowData();
        var res = this.gridApi.updateRowData({ add: [newItem1, newItem2, newItem3, newItem4, newItem5] });
        this.printResult(res);
        this.getPortfolioValue();
      }
    }

    addItemsAtIndex() {
        var newItems = [this.createNewRowData(), this.createNewRowData(), this.createNewRowData()];
        var res = this.gridApi.updateRowData({
          add: newItems,
          addIndex: 2
        });
        this.printResult(res);
    }

    updateItems() {
        var itemsToUpdate: any = [];
        this.gridApi.forEachNodeAfterFilterAndSort(function(rowNode, index) {
          if (index >= 5) {
            return;
          }
          var data = rowNode.data;
          data.price = Math.floor(Math.random() * 20000 + 20000);
          itemsToUpdate.push(data);
        });
        var res = this.gridApi.updateRowData({ update: itemsToUpdate });
        this.printResult(res);
    }

    onInsertRowAt2() {
        var newItem = this.createNewRowData();
        var res = this.gridApi.updateRowData({
          add: [newItem],
          addIndex: 2
        });
        this.printResult(res);
    }

    onRemoveSelected = () => {
        var selectedData = this.gridApi.getSelectedRows();
        var res = this.gridApi.updateRowData({ remove: selectedData });
        this.printResult(res);
    }

    getTopBar = () => {
      var showValue: number = 0;
      if(this.props.keyLiveData !== null) {
        showValue = this.state.portfolioValue;
      }
    
      return (  
        <div style={{ width: "100%", display: "flex"}}>
          <Typography style={{padding: "12px", backgroundColor:"#2d2d2d",color: "#DC9202"}}>
            Total Portfolio Value: USD$ {showValue}
          </Typography> 
          <IconButton>
            <AddIcon onClick={this.onAddRow } color="primary" fontSize="small"/>
          </IconButton>
          <IconButton >
            <DeleteIcon onClick={this.onRemoveSelected} color="primary"  fontSize="small"/>
          </IconButton>
          <IconButton >
            <ClearAllIcon onClick={this.clearData} color="primary"  fontSize="small"/>
          </IconButton>
      </div>
      )
    }

    render() {
      if (this.props.keyLiveData !==  null && this.state.initialized === false) {
        console.log(this.props.keyLiveData)
        this.generateInitialData();
        this.setState({initialized: true})
      }
      
        return (
          <React.Fragment>
            <PortfolioAddDialog open={this.state.dialogOpen}/>
            {this.getTopBar()}
            <div
                id="myGrid"
                style={{
                  height: "91vh",
                  width: "100vw - 65px"
                }}
                className="ag-theme-dark"
            >
                <AgGridReact
                  columnDefs={this.state.columnDefs}
                  rowData={this.state.rowData}
                  animateRows={true}
                  rowSelection={this.state.rowSelection}
                  onGridReady={this.onGridReady}
                  frameworkComponents={this.state.frameworkComponents}
                  defaultColDef={this.state.defaultColDef}
                />
          </div>
        </React.Fragment>
        );
      }
    
    
    createNewRowData() {
      const randomIndex: number = this.getRandomInt(0, (this.props.keyLiveData.length -1));
      const randomTicker: IKeyLiveDataObject = this.props.keyLiveData[randomIndex];
      console.log(randomIndex);
      const randomQuantity: number = this.getRandomInt(1,100);
      const tickerPrice: number =  randomTicker.liveprice;
      var newData = {
        tradeId: this.getRandomTradeId(),
        tradeDate: this.getCurrentTime(),
        symbolCaps: randomTicker.symbolCaps,
        name: randomTicker.name,
        price: tickerPrice,
        quantity: randomQuantity,
        value: Math.ceil(tickerPrice * randomQuantity),
        beta: randomTicker.beta,
        eps: randomTicker.eps,
        peratio: randomTicker.peratio,
        yield: randomTicker.yield,
        dividend: randomTicker.dividend
      }
      this.newCount++;
      return newData;
    }
    getCurrentTime = () => {
      // const currTimeISO = moment().toISOString();
      var dateComponent = moment().format('YYYY-MM-DD');
      var timeComponent = moment().format('HH:mm:ss');
      const currTimeStr = dateComponent + " " + timeComponent;
      return currTimeStr;
    }
    getRandomTradeId = () => {
      const randomNum: number = this.getRandomInt(10001,99999);
      console.log(randomNum)
      const randomTradeId = "N" + randomNum;
      console.log(typeof randomTradeId);
      return randomTradeId;
    }
    getRandomInt = (min: number, max: number) => {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    printResult(res) {
      console.log("---------------------------------------");
      if (res.add) {
        res.add.forEach(function(rowNode) {
          console.log("Added Row Node", rowNode);
        });
      }
      if (res.remove) {
        res.remove.forEach(function(rowNode) {
          console.log("Removed Row Node", rowNode);
        });
      }
      if (res.update) {
        res.update.forEach(function(rowNode) {
          console.log("Updated Row Node", rowNode);
        });
      }
    }

}


  
