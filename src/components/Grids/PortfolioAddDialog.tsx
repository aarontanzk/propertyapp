import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import withMobileDialog from '@material-ui/core/withMobileDialog';

export class PortfolioAddDialog extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state={
            open: false
        }
    }
    getDerivedStateFromProps(props, state) {
        if (state.open === false) {
            this.setState({open: false})
        } else if (state.open === true) {
            this.setState({open: true})
        }
        this.setState({open:false})
    }

    handleClose = () => {
        this.setState({open: false})
      }
    render(){

    return (
      
          <Dialog
            open={this.props.open} 
            onClose={this.handleClose}
          >
            <DialogTitle id="responsive-dialog-title">{"Use Google's location service?"}</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Let Google help apps determine location. This means sending anonymous location data to
                Google, even when no apps are running.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose}  color="primary">
                Disagree
              </Button>
              <Button onClick={this.handleClose}   color="primary" autoFocus>
                Agree
              </Button>
            </DialogActions>
          </Dialog>
      );
    }


}
 

