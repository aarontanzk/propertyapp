import React from 'react';
import { css } from '@emotion/core';
// First way to import
import { PulseLoader } from 'react-spinners';
// Another way to import
import Typography from "@material-ui/core/Typography";
 
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;
 
export class Loading extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }
  render() {
    return (
        <div style={{ 
            position: "absolute",
            width: "600px",
            height: "400px",
            top: "50%",
            left: "50%",
            margin: "-100px 0 0 -150px",
            background: "transparent",
            display: "flex"
          }}>
            <PulseLoader
            sizeUnit={"px"}
            size={50}
            color={'#DC9202'}
            loading={this.state.loading}
            />
      </div> 
    )
  }
}