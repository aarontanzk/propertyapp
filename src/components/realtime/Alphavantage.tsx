/**
 * Init Alpha Vantage with your API key.
 *
 * @param {String} key 
 *   Your Alpha Vantage API key.
 */
// @ts-ignore 

const alpha = require('alphavantage')({ key: '392PM7H6LAUQ50C3' });
 
// Simple examples
alpha.data.intraday(`msft`).then((data: any) => {
  console.log(data);
});
 
alpha.data.batch([`msft`, `aapl`]).then((data: any) => {
  console.log(data);
});
 
alpha.forex.rate('btc', 'usd').then((data: any) => {
  console.log(data);
})
 
alpha.crypto.daily('btc', 'usd').then((data: any) => {
  console.log(data);
})
 
alpha.technical.sma(`msft`, `daily`, 60, `close`).then((data: any) => {
  console.log(data);
})
 
alpha.performance.sector().then((data: any) => {
  console.log(data);
});