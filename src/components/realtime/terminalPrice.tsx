import React from 'react';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { dowjones_symbols } from '../../data/staticdata';


export class TerminalPrice extends React.Component<any, any> {
    constructor(props: any){
      super(props);
      this.state = {
          selectedCurrPrice: 0,
          allCurrPrice: this.props.allCurrPrice,
          selectedStock: this.props.selectedStock
      }

    }
 
    getCurrPriceObj = () => {
        console.log(this.props.allCurrPrice);
        console.log(this.props.selectedStock)
        if (this.props.allCurrPrice !== null){
         // console.log(this.props.selectedStock);
          let index;
          let counter = 0;
          for (var i in dowjones_symbols) {
            // console.log(i)
           // console.log(this.state.selectedStock.symbol);
           // console.log(dowjones_symbols[i]);
            if (this.props.selectedStock.symbol === dowjones_symbols[i]) {
              index = counter
            } else {
              counter++;
            }
          }
          console.log(index)
        console.log(this.props.allCurrPrice[index]);
        let alphaObj = this.props.allCurrPrice[index]
        //console.log(this.state!.allCurrPrice[index]["2. price"])
        //return this.props.allCurrPrice[index];
          let values = this.parseAlphaObjectToArr(alphaObj);
          console.log(values)
          return values;
        }
        return ["","","",""];
      }

      parseAlphaObjectToArr = (obj: any) => {
        var values = Object.values(obj);
        return values;
      }
    
    render(){
        console.log(this.props)
        if (this.props.allCurrPrice !== null || this.props.allCurrPrice !== undefined) {

            //this.selectedCurrPriceArr = this.getCurrPriceObj();
            var values = this.getCurrPriceObj();
            //console.log(this.selectedCurrPriceArr)
            return (
                <React.Fragment>
                    <Typography align="left" variant="h6" style={{ color: "#2d2d2d", backgroundColor: "grey", width:"150px", paddingLeft:"5px", borderRadius: "5px", marginBottom: "5px"}}>
                        {this.props.selectedStock.country} {this.props.selectedStock.exchange} 
                    </Typography>
                    <Typography color="primary" align="left" variant="h4">
                        {this.props.selectedStock.name}
                    </Typography>
                    <Divider style={{backgroundColor: "#DC9202", marginTop: "5px", height: "2px"}} />
                    <Typography color="primary" align="right" variant="h6" style={{paddingTop:"30px"}}>

                    </Typography>
                    <Typography color="primary" align="right" variant="h2" style={{paddingTop:"30px"}}>
                        {values[1]}
                    </Typography>
                    <Typography color="primary" align="right" variant="h2" style={{paddingTop:"30px"}}>
                     
                    </Typography>
                    <Typography color="primary" align="right" variant="h6" style={{paddingTop:"30px"}}>
                          Last Updated: {values[3]}
                    </Typography>
                </React.Fragment>
                );
        } else {
            return (
                <div>
                    loading...
                </div>
            )
        }

    
    }
  }
  
  