import React from 'react';
import Sidebar from '../Sidebar';
import '../../App.css';
import Grid from '@material-ui/core/Grid';
import NativeSelect from '@material-ui/core/NativeSelect';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { dowjones_objects, dowjones_symbols } from '../../data/staticdata';
import Typography from '@material-ui/core/Typography';
import StockGraph from './Victorycharts';
import {TerminalPrice} from './terminalPrice';

const alpha = require('alphavantage')({ key: '84AUAFSDFTSY9XUT' });

export default class TerminalHOC extends React.Component<any, any> {
    stockPrice: any;

    constructor(props: any){
      super(props);
      this.state = {
        stock: 'axp',
        selectedStock: dowjones_objects[0],
        allCurrPrice: null,
      };
      //his.stockPrice = this.displayPrice();
      this.getAllCurrentPricesDowJones();
     // this.getCurrPrice();
    }

    handleChange = (name: any) => (event: any) => {
      // console.log(name);
      var currStock = dowjones_objects.find( obj => obj.name === event.target.value );
      //var currPrice = this.displayPrice();
      console.log('handle change');
      console.log(currStock)
      this.setState({selectedStock: currStock, [name]: event.target.value});
    };

    getAllCurrentPricesDowJones = () => {
      alpha.data.batch(dowjones_symbols).then((data: any) => {
        this.setState({allCurrPrice: data["Stock Quotes"]})
      });
    }

    render(){
      console.log(this.state.stock);
      console.log(this.state.selectedStock);
      //console.log(msft_historical)

      

     // this.stockPrice = this.displayPrice();
      console.log(this.state.allCurrPrice);
     
    return (
      <React.Fragment>
        <Sidebar/>
        <div className="Container" style={{height: "95vh"}}>
          <Grid style={{paddingLeft: "20px"}} container spacing={24}>
            <Grid item xs={3}>
              <Typography color="primary" align="right" variant="h6" style={{paddingTop: "27px"}}>
                TICKER:
              </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                  id="outlined-select-currency-native"
                  select
                  // label="Native select"
                  value={this.state.stock}
                  onChange={this.handleChange('stock')}
                  SelectProps={{
                    native: true,
                  }}
                  // helperText="Please select your currency"
                  margin="normal"
                  variant="outlined"
                  >
                  {dowjones_objects.map(option => (
                    <option key={option.symbol} value={option.name}>
                      {option.name}
                    </option>
                  ))}
              </TextField>
            </Grid>
            <Grid item xs={5}>
                <TerminalPrice selectedStock = {this.state.selectedStock} allCurrPrice = {this.state.allCurrPrice} />
            </Grid>
            <Grid item xs={7}>
              <StockGraph ticker={this.state.selectedStock.symbol}/>
            </Grid>
            <Grid item xs={12}>
              <Typography color="primary" align="right" variant="h6">
                Note: AlphaVantage API max calls 5 per minute.   If no data loads, please wait and try again.
              </Typography>
            </Grid>
        
          </Grid>
        </div>
      </React.Fragment>
    );
  }
  }
  
  // <StockGraph ticker={this.state.selectedStock.symbol}/>
