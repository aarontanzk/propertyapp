 /* tslint:disable */ 
import * as React from "react";
import { VictoryPie, VictoryChart, VictoryLine, VictoryAxis } from "victory";
import {historical} from '../../data/dowjones';
import Button from '@material-ui/core/Button';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import { Typography } from "@material-ui/core";

const data = [
  { x: "17-5-2016", y: "126" },
  { x: "16-5-2016", y: "128" },
  { x: "15-5-2016", y: "126" },
  { x: "14-5-2016", y: "128 "}
]

interface ITickerObject  {
  data: string
}
export default class StockGraph extends React.Component<any, any> {
  constructor(props: any){
    super(props);
    this.state = {
      ticker: this.props.ticker,
      timerange: '2019'
    };
  }
  timerangeToYtd = () => {
    this.setState({timerange: "2019"})
  }

  timerangeTo3Yr = () => {
    this.setState({timerange: "2017"})
  }

  parseData_close_date = (ticker: any, timerange: any) => {
    var arr: string[] = [];
    var data = historical[ticker][timerange]["Time Series (Daily)"]
    var keys = Object.keys(data);
    var values =  Object.values(data);
    // console.log(data);
    
    for(var x in values) {
     var innerValues =  Object.values(values[x]);
      let obj: any = {};
      obj['y'] = parseFloat(innerValues[3]);
      obj['x'] =  new Date(keys[x]);
      arr.push(obj);
     
    }
    console.log(arr);
    return arr;
  }

  render() {
    console.log(this.props.ticker);
    var data2 = this.parseData_close_date(this.props.ticker,this.state.timerange);
   // console.log(data2);
    //console.log(data);
    console.log(this.state.timerange)
    return(
      <React.Fragment>
        <div style={{textAlign:"right"}}>
          <Button onClick={this.timerangeToYtd}>
            <Typography style={{padding: "10px", width: "100px", backgroundColor:"#DC9202",color: "#2d2d2d"}}>
              YTD
            </Typography>
          </Button>
          <Button onClick={this.timerangeTo3Yr}>
            <Typography style={{padding: "10px", width: "100px", backgroundColor:"#DC9202",color: "#2d2d2d"}}>
              3 Years
            </Typography>
          </Button>
        </div>
      <VictoryChart>
        <VictoryLine  
          interpolation="natural"
          animate={{
            duration: 200,
            onLoad: { duration: 1000 }
          }}
          scale={{ x:"time", y:"linear" }}
          style={{  
            parent: { border: "1px solid #ffffff",fontSize:5} ,
            data: { stroke: "#c43a31", strokeWidth: 1, strokeLinecap: "round" }, 
            labels:{fill:"#ffffff", color:"pink", fontSize:5} }}
         data={data2} />
    
            </VictoryChart>
      </React.Fragment>
    );
  }
}

/**
 * //radial bar chart
const colors = {
   pink: ["#CB5599", "#5E6063"],
   teal: ["#49C6B7", "#5E6063"]
};

<VictoryChart>
   {
      data.length > 1 ? <VictoryLine
         data={data}
         x="time"
         style={{
            data: {stroke: colors[color], strokeWidth:4},
         }}
         y={(datum) => datum.value}
      /> : null
   }
   <VictoryAxis
      // x
      tickValues={xRange.length > 1 ? xRange : ["8:00 PM"]}
      tickFormat={(tick) => {
         if (data.length < 1){
            return tick;
         }
         const time = data[tick-1].time.split(":");
         return formatTime(time);
      }}
      style={{
         axis: {stroke: "#ffffff"},
         ticks: {stroke: "#ffffff"},
         tickLabels: {fontSize: 12, padding: 30, stroke:"#EAEDEF"}
      }}
   />
   <VictoryAxis
      // y
      dependentAxis
      tickValues={yRange.length > 1 ? yRange : [0, 3, 6]}
      style={{
         axis: {stroke: "none"},
         grid: {stroke:colors.mediumGray},
         tickLabels: {fontSize: 12, padding: 30,   stroke:colors.lightGray}
      }}
   />
  }
  </VictoryChart>
 */
