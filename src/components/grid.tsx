import React from 'react';
import {AgGridReact} from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-dark.css';

import '../App.css';
import {KeyData} from '../data/KeyData';
import {colDefs} from '../data/columns';

export default class AgGrid extends React.Component<any, any> {
    private gridApi: any;
    private gridColumnApi: any;
    
    constructor(props: any){
      super(props);

      this.state = {
        defaultColDef: {
            enableRowGroup: true,
            enablePivot: true,
            enableValue: true,
            width: 100,
            sortable: true,
            resizable: true,
            filter: true
        },
        columnDefs: colDefs,
        rowData: KeyData
      }
    }

    onGridReady = params => {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    render(){

    return (
      <React.Fragment>
        <div
          className="ag-theme-dark"
          style={{
            height: '100vh',
            width: '100vw',
            // marginLeft: "66px"
          }}
        >
          <AgGridReact
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}
            defaultColDef={this.state.defaultColDef}
            animateRows={true}
            //sideBar={this.state.sideBar}
            //statusBar={this.state.statusBar}
            //enableRangeSelection={true}
            onGridReady={this.onGridReady}
              >
          </AgGridReact>
        </div>
      </React.Fragment>
    );
  }
}
  