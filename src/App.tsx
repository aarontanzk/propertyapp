import React from 'react';
import './App.css';
import Container from './Container';
import Sidebar from './components/Sidebar';

class App extends React.Component<any, any> {
  render(){
    return (
      <React.Fragment>
        <Sidebar/>
        <Container />
      </React.Fragment>
    );
  }
}

export default App;
