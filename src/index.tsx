import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import TerminalHOC from './components/realtime/terminalHOC';
import Main from './components/main';
import { MuiThemeProvider } from '@material-ui/core/styles';
import {theme} from './muitheme/theme';
import Grid from './components/grid';
import {LayoutHOC} from './components/layouts/LayoutHOC';
import PortfolioGrid from './components/Grids/PortfolioGrid';
import {LivepriceHOC} from './components/Grids/LivepriceHOC';
import {About} from './components/About';

import {LicenseManager} from "ag-grid-enterprise";
LicenseManager.setLicenseKey("Evaluation_License-_Not_For_Production_Valid_Until_29_July_2019__MTU2NDM1NDgwMDAwMA==d729d2987753931ba4f89010cf964f7b");

const routing = (
    <MuiThemeProvider theme={theme}>
        <Router>
            <Route exact path="/" component={LayoutHOC} />
            <Route path="/dashboard" component={LayoutHOC} />
            <Route path="/terminal" component={TerminalHOC} />
            <Route path="/about" component={About} />
        </Router>
    </MuiThemeProvider>
  )

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
