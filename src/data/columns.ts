export const colDefs = [
    {
        headerName: "Symbol", 
        field: "symbolCaps", 
        width: 100, 
        maxWidth:100, 
        editable: false, 
        //cellEditor: 'agSelectCellEditor',
        /*
        cellEditorParams : {
            values: ['English', 'Spanish', 'French', 'Portuguese', '(other)']
        }
        */
    },
    {
        headerName: "Name", 
        field: "name", 
        width: 300, 
        editable: false, 
    },
    {
        headerName: "Live Price", 
        field: "liveprice", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "52 Week High", 
        field: "high52week", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "52 Week Low", 
        field: "low52week", 
        width: 100, 
        editable: false, 
    },   
    {
        headerName: "Beta", 
        field: "beta", 
        width: 100, 
        editable: false, 
    },  
    {
        headerName: "P/E Ratio", 
        field: "peratio", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "EPS", 
        field: "eps", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "% Yield", 
        field: "yield", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "Dividend (USD)", 
        field: "dividend", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "Market Cap", 
        field: "marketcap", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "AVG VOL", 
        field: "avgvol", 
        width: 100, 
        editable: false, 
    },
]


export const portfolioColDefs = [
    {
        headerName: "Trade Id",
        field: "tradeId",
        width: 100,
        maxWidth: 100,
        editable: false,
    },
    {
        headerName: "Trade Date",
        field: "tradeDate",
        width: 220,
        maxWidth: 600,
        editable: false,
    },
    {
        headerName: "Symbol", 
        field: "symbolCaps", 
        width: 100, 
        maxWidth:100, 
        editable: false
    },
    {
        headerName: "Ticker Name", 
        field: "name", 
        width: 500, 
        editable: false, 
    },
    {
        headerName: "Price (USD)", 
        field: "price", 
        width: 150, 
        editable: false, 
    },
    {
        headerName: "Quantity", 
        field: "quantity", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "Value (USD)", 
        field: "value", 
        width: 150, 
        editable: false, 
    },
    {
        headerName: "Beta", 
        field: "beta", 
        width: 100, 
        editable: false, 
    },  
    {
        headerName: "P/E Ratio", 
        field: "peratio", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "EPS", 
        field: "eps", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "% Yield", 
        field: "yield", 
        width: 100, 
        editable: false, 
    },
    {
        headerName: "Dividend (USD)", 
        field: "dividend", 
        width: 100, 
        editable: false, 
    },
]